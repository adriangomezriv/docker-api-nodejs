FROM ubuntu

RUN apt-get update -y && apt-get upgrade -y
RUN apt-get -y install apt-utils
RUN apt-get install curl -y
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash -
RUN apt-get install -y nodejs

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY . .

CMD ["npm","start"]

const { Router } = require("express");
const router = Router();
const { vone_image } = require("../controllers/images.controller");

router.get("/images/:id", vone_image);

router.delete("/images/:id", done_image);

module.exports = router;

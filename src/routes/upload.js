const { Router } = require("express");
const router = Router();
const { view_form, send_image } = require("../controllers/upload.controller");

//llamo al modelo de las images
const Image = require("../models/image");

router.get("/upload", view_form);

router.post("/upload", send_image);

module.exports = router;

const express = require("express");
const morgan = require("morgan");
const path = require("path");
const multer = require("multer");
const uuid = require("uuid/v4");

//initializing server app
const app = express();
//initializing database
require("./database");

//settings
app.set("port", process.env.PORT || 8080);
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

//midlewares
//show clients requests
app.use(morgan("dev"));

//permite entender al servidor lo que envian los formularios del frontend
//el extended false significa que no voy a enviar desde el frontend algo complejo
//ya que las imagenes las procesaremos con el modulo multer
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, "public")));

//multer nos da un metodo diskstorage para configurar como se guardan los archivos
const storage = multer.diskStorage({
  destination: path.join(__dirname, "public/img/uploads"),
  filename: (req, file, cb, filename) => {
    cb(null, uuid() + path.extname(file.originalname));
  }
});
//procesa las imagenes que llegan del frontend y las guarda en la carpeta especificada
app.use(multer({ storage: storage }).single("image"));

//call routes
app.use(require("./routes/index"));
app.use(require("./routes/upload"));

//export app to be used in other file(index.js)
module.exports = app;

const mongoose = require("mongoose");

const URI = "mongodb://localhost/red-social";
mongoose
  .connect(URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
  })
  .then(db => console.log("DB is conected"))
  .catch(err => console.error(err));

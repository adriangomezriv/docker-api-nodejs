//call to server defined in extern file
const app = require("./app");

//starting server
app.listen(app.get("port"), () => {
  console.log(`server on port ${app.get("port")}`);
});

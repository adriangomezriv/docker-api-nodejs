export const view_form = (req, res) => {
  res.render("upload");
};

export const send_image = async (req, res) => {
  const nuevaImagen = new Image();
  nuevaImagen.title = req.body.title;
  nuevaImagen.description = req.body.description;
  nuevaImagen.filename = req.file.filename;
  nuevaImagen.path = req.file.path;
  nuevaImagen.originalname = req.file.originalname;
  nuevaImagen.mymetype = req.file.mimetype;
  nuevaImagen.size = req.file.size;
  await nuevaImagen.save();
  res.redirect("/");
};

const { Schema, model } = require("mongoose");

//definimos los datos que queremos guardar de la imagen(el esquema)
const imageSchema = new Schema({
  title: { type: String, required: true },
  description: { type: String, required: true },
  filename: { type: String },
  path: { type: String },
  originalname: { type: String },
  mimetype: { type: String },
  size: { type: Number },
  created_at: { type: Date, default: Date.now() }
});

//con el modelo le damos un nombre al esquema para ser usado en cualquier
//arte de la app
module.exports = model("Image", imageSchema);
